///////////////////////////////////////////////////////////////////////////////
/////        University of Hawaii, College of Engineering
///// @brief Lab 7c - @todo: hello3 - EE 205 - Spr 2022
/////
///// @file hello3.cpp
/////
///// @version 1.0
/////
///// @author Xiaokang Chen <xiaokang@hawaii.edu>
///// @date 24 Feb 2022
///////////////////////////////////////////////////////////////////////////////


#include <iostream>

using namespace std;


class Cat {

      public:

         void sayHello() {

            cout << "Meow Meow" << endl;

         }

};

int main() {

   Cat myCat;
   myCat.sayHello();

}

///////////////////////////////////////////////////////////////////////////////
/////        University of Hawaii, College of Engineering
///// @brief Lab 7c - hello1 - EE 205 - Spr 2022
/////
///// @file hello1.cpp
/////
///// @version 1.0
/////
///// @author Xiaokang Chen <xiaokang@hawaii.edu>
///// @date 24 Feb 2022
///////////////////////////////////////////////////////////////////////////////


#include <iostream>

using namespace std;

int main() {

   cout << "Hello, World" << endl;

}
